/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package csvrec

import (
	"bitbucket.org/srosencrantz/csv"
	"fmt"
	"os"
	"strings"
)

func Int2Str(num int) string {
	return strings.TrimSpace(fmt.Sprintf("%d", num))
}

func Flt2Str(num float64) string {
	return strings.TrimSpace(fmt.Sprintf("%12.8e", num))
}

func Bool2Str(flag bool) string {
	temp := 0
	if flag {
		temp = 1
	}
	return Int2Str(temp)
}

func PadRecords(records RecordList) {
	// determine the max # of columns (each row could be different
	maxCol := 0
	for _, rec := range records {
		if len(rec) > maxCol {
			maxCol = len(rec)
		}
	}
	// for each column find the longest string (maxField) then pad every string in the column to be maxField+1
	for i := 0; i < maxCol; i++ {
		maxField := 0
		for _, rec := range records {
			if len(rec) > i {
				if len(rec[i]) > maxField {
					maxField = len(rec[i])
				}
			}
		}
		for j, rec := range records {
			if len(rec) > i {
				padLen := maxField + 1
				fmtStr := fmt.Sprintf("%ds", padLen)
				if j == 0 || i == 0 {
					fmtStr = "%-" + fmtStr
				} else {
					fmtStr = "%" + fmtStr
				}
				rec[i] = fmt.Sprintf(fmtStr, rec[i])
			}
		}
	}
}

func WriteCsvRecordFile(name string, records RecordList) (err error) {
	var f *os.File
	if f, err = os.Create(name); err != nil {
		return err
	}
	wcsv := csv.NewWriter(f)
	wcsv.WriteAll(records)
	if err = wcsv.Error(); err != nil {
		return err
	}
	return nil
}
