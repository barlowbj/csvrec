#CSVREC#

Is a library for reading and writing files where each line is a record.  Each record is a comma seperated list
of fields, with the first field being a record identifier. "//" at the begining of a line indicates the line
is a comment. Blank lines are ignored.

For example a file might contain:

```
// Example File 

// Shotline, lx, ly, lz, dx, dy, dz, v
Shotline, 971.998, -324.021, -207.936, 0.0, 0.0, 1.0, 100.0
Shotline, 1133.2, -594.528, -222.62, 0.0, 0.0, 1.0, 100.0  

// Sphere, type, x, r, rs, dpcent, sdcent, dpsurf, sdsurf
Sphere, shell, 5.0, 10.0, 10.0, 0.9, .16, 0.5, 0.16

// Cylinder, type,  x1, ri1, ro1, dpc1, sdc1, dps1, sds1,   x2,  ri2,  ro2, dpc2, sdc2, dps2, sds2
Cylinder,   shell, 0.0, 1.0, 5.0,  0.9, 0.16,  0.9, 0.16, 50.0, 30.0, 40.0,  0.9, 0.16,  0.9, 0.16

// Nnet, type, thetaDir 
Nnet, shell, thetadir
Nnet, crack, myCrackDamageThetas
Nnet, thermal, myThermalThetas
```
## Reading ##
This file contains four types of records and multiple instances of some records.  The function call
```recmap, err := ReadCsvRecordFile("examplefile") ```
would return a map where recmap["Shotline"] would return a [2][8]string with all the records.  The functions
ReadInt, ReadFloat, and ReadBool are provided as a short hand  to convert individual fields to the desired type
in a consistant manner.

## Writing ##
The WriteCsvRecordFile function takes writes a list of records ([][]string) to a file.
