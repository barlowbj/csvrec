/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package csvrec

import (
	"bitbucket.org/srosencrantz/csv"
	"os"
	"strconv"
	"strings"
)

type Record []string

type RecordList [][]string

type RecordMap map[string]RecordList

// ReadCsvRecordFile reads a csv record file and returns a map of the records
func ReadCsvRecordFile(name string) (recordMap RecordMap, err error) {
	var f *os.File
	if f, err = os.Open(name); err != nil {
		return nil, err
	}

	rcsv := csv.NewReader(f)
	rcsv.Comma = ','
	rcsv.Comment = '/'
	rcsv.FieldsPerRecord = -1

	var records RecordList
	records, err = rcsv.ReadAll()
	if err != nil {
		return nil, err
	}

	records.TrimSpace()
	recordMap = make(RecordMap)

	for _, r := range records {
		recordMap[r[0]] = append(recordMap[r[0]], r)
	}

	return recordMap, nil
}

func (records RecordList) TrimSpace() {
	for i := range records {
		for j := range records[i] {
			records[i][j] = strings.TrimSpace(records[i][j])
		}
	}
}

func ReadInt(field string) (num int, finalErr error) {
	temp, err := strconv.ParseInt(field, 10, 0)
	return int(temp), err
}

func ReadFloat(field string) (num float64, finalErr error) {
	return strconv.ParseFloat(field, 64)
}

func ReadBool(field string) (out bool, err error) {
	var tempInt int
	out = true
	tempInt, err = ReadInt(field)
	if tempInt == 0 {
		out = false
	}
	return out, err
}
